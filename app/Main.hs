module Main where

import qualified System.IO as IO

import Options.Applicative (execParser)

import qualified Data.ByteString.Lazy as BSL

import Opts.Parsers.CLI
import qualified Opts.Types as O

import Give (give')
import Pick (pick')

main :: IO ()
main = do
  opts <- execParser optsParser
  src <- BSL.getContents
  case O.cmd opts of
    (O.Give opts') -> putStrLn' $ give' opts' src
    (O.Pick opts') -> putStrLn' $ merge $ pick' opts' src
  where
    putStrLn' = BSL.hPut IO.stdout
    merge :: Either a a -> a
    merge (Left x) = x
    merge (Right x) = x
