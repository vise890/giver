# The Giver

> It's the choosing that's important, isn't it?
>
> — <cite>Lois Lowry, The Giver</cite>

Giver gives IDs to stuff and lets you pick stuff out by IDs. By default,
"stuff" is words (as in text separated by space).


## Usage

```bash
$ giver --help
# ...
$ giver give --help
# ...
$ giver pick --help
# ...

$ ls
# foo bar baz beep.txt boop.md

$ ls | giver
# 1 foo
# 2 bar
# 3 baz
# 4 beep.txt
# 5 boop.md

$ ls | giver give | giver pick 1 3-5
# foo
# baz
# beep.txt
# boop.md
```


### Json

```bash
$ ls | giver give --out=json
# {"1": "foo", "2": "bar", "3": "baz", ...}

$ ... | giver pick --in=json 1
# foo

$ ... | giver pick --in=json --out=json 1
# {"1": "foo"}

$ ... | giver pick --in=json --out=idlist 1
# 1 foo
```


# Building

First, install [stack](https://docs.haskellstack.org/en/stable/README/). Then:

```bash
stack setup
stack install
~/.local/bin/giver

# or add ~/.local/bin to $PATH and
giver
```


# The project

This should be considered pre-alpha quality. To see where it's going, take a
look at the [`BLUEPRINT.md`](./BLUEPRINT.md)


# Integrating with ZSH

[![asciicast](https://asciinema.org/a/87757.png)](https://asciinema.org/a/87757)
