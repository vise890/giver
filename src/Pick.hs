module Pick
  ( pick
  , pick'
  ) where

import qualified Control.Arrow (left)
import qualified Data.ByteString.Lazy.UTF8 as UTF8

import qualified Data.ByteString.Lazy as BSL

import qualified Opts.Types as O

import IDList

pick :: [ID] -> [IDElem] -> [IDElem]
pick ids = filter ((`elem` ids) . fst)

pick' :: O.PickOpts -> BSL.ByteString -> Either BSL.ByteString BSL.ByteString
pick' opts = fmap (showPickOut opts . pick ids) . readIDList' opts
  where
    ids = O.pickIds opts

showPickOut :: O.PickOpts -> [IDElem] -> BSL.ByteString
showPickOut opts =
  case O.pickOutFormat opts of
    O.OutIDList -> showAsIDList
    O.OutJSON -> showAsJSON
    O.OutBare -> showBare

readIDList' :: O.PickOpts -> BSL.ByteString -> Either BSL.ByteString [IDElem]
readIDList' opts = leftToBS . readF
  where
    readF =
      case O.pickInFormat opts of
        O.InJSON -> readJSONIDList
        O.InIDList -> readIDList
    leftToBS = Control.Arrow.left UTF8.fromString
