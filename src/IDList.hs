{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module IDList
  ( ID
  , IDElem
  , showAsIDList
  , showAsJSON
  , showBare
  , readIDList
  , readJSONIDList
  ) where

import qualified Data.HashMap.Strict as M

import qualified Data.Aeson as J

import qualified Data.Attoparsec.Text.Lazy as A

import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TLE

type ID = Int

type IDElem = (ID, TL.Text)

-- TODO: make these into Show/Read instances?
showBare :: [IDElem] -> BSL.ByteString
showBare = TLE.encodeUtf8 . TL.unlines . map snd

showAsIDList :: [IDElem] -> BSL.ByteString
showAsIDList = TLE.encodeUtf8 . TL.unlines . map toLine
  where
    toLine tup = TL.concat [TL.pack $ show (fst tup), " ", snd tup]

-- FIXME: I suspect I'm overcomplicating things here..
showAsJSON :: [IDElem] -> BSL.ByteString
showAsJSON = J.encode . M.fromList . map toStrKey
  where
    toStrKey (i, v) = (show i, v)

readIDList :: BSL.ByteString -> Either String [IDElem]
readIDList = A.parseOnly (idList <* A.endOfInput) . TL.toStrict . TLE.decodeUtf8

idList :: A.Parser [IDElem]
idList = idElem `A.sepBy` A.endOfLine <* A.skipSpace

idElem :: A.Parser IDElem
idElem = do
  i <- A.many1 A.digit
  A.skipWhile A.isHorizontalSpace
  n <- A.takeTill A.isEndOfLine
  return (read i, TL.fromStrict n)

readJSONIDList :: BSL.ByteString -> Either String [IDElem]
readJSONIDList src =
  let decodedTups :: Maybe [(String, TL.Text)]
      decodedTups = fmap M.toList (J.decode src)
      toIDListElem (k, v) = (read k, v)
  in case decodedTups of
       Just tups -> Right $ map toIDListElem tups
       Nothing -> Left "Could not parse JSON input"
