module Give
  ( give
  , give'
  ) where

import qualified Data.ByteString.Lazy as BSL
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TLE

import qualified Opts.Types as O

import IDList

give :: BSL.ByteString -> [IDElem]
give = zip [1 ..] . TL.words . TLE.decodeUtf8

showGiveOut :: O.GiveOpts -> [IDElem] -> BSL.ByteString
showGiveOut opts =
  case O.giveOutFormat opts of
    O.OutIDList -> showAsIDList
    O.OutJSON -> showAsJSON
    O.OutBare -> showBare

give' :: O.GiveOpts -> BSL.ByteString -> BSL.ByteString
give' opts = showGiveOut opts . give
