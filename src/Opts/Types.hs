module Opts.Types
  ( Opts(..)
  , Cmd(..)
  , GiveOpts(..)
  , ID
  , PickOpts(..)
  , InFormat(..)
  , OutFormat(..)
  ) where

data Opts = Opts
  { cmd :: Cmd
  } deriving (Read, Show)

data Cmd
  = Give GiveOpts
  | Pick PickOpts
  deriving (Read, Show)

data GiveOpts = GiveOpts -- TODO: regex :: Maybe String
  { giveOutFormat :: OutFormat
  } deriving (Read, Show)

-- NOTE: Bare is useless as an out format for 'give', but, yolo, i'm not gonna
--       have separate OutFormats for give and pick. I'm already separating in
--       and out ffs.
type ID = Int

data PickOpts = PickOpts
  { pickIds :: [ID]
  , pickInFormat :: InFormat
  , pickOutFormat :: OutFormat
  } deriving (Read, Show)

data InFormat
  = InIDList
  | InJSON
  deriving (Eq)

instance Read InFormat where
  readsPrec _ =
    let parseToken "idlist" = Just InIDList
        parseToken "json" = Just InJSON
        parseToken _ = Nothing
    in mkReadS parseToken

instance Show InFormat where
  show InIDList = "idlist"
  show InJSON = "json"

data OutFormat
  = OutIDList
  | OutJSON
  | OutBare
               -- TODO: SubstituteOut
  deriving (Eq)

instance Read OutFormat where
  readsPrec _ =
    let parseToken "idlist" = Just OutIDList
        parseToken "json" = Just OutJSON
        parseToken "bare" = Just OutBare
        parseToken _ = Nothing
    in mkReadS parseToken

instance Show OutFormat where
  show OutIDList = "idlist"
  show OutJSON = "json"
  show OutBare = "bare"

mkReadS :: (String -> Maybe a) -> ReadS a
mkReadS parseToken =
  let parseTokens [] = []
      parseTokens [w] =
        case parseToken w of
          Just x -> [(x, "")]
          Nothing -> []
      parseTokens (w:ws) =
        case parseToken w of
          Just x -> [(x, unwords ws)]
          Nothing -> []
  in parseTokens . words
