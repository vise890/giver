module Opts.Parsers.CLI
  ( optsParser
  ) where

import Data.Monoid
import Options.Applicative
import Opts.Types

import Opts.Parsers.IDs

defaultOpts :: Opts
defaultOpts = Opts {cmd = Give GiveOpts {giveOutFormat = OutIDList}}

optsParser :: ParserInfo Opts
optsParser =
  info
    (helper <*> optsParser' <|> pure defaultOpts)
    (fullDesc <> header "giver - give IDs to stuff and pick stuff out by ID")
  where
    optsParser' = Opts <$> subparser (giveCmd <> pickCmd)
    giveCmd =
      command
        "give"
        (info
           (helper <*> (Give <$> giveOpts))
           (progDesc "give IDs to words from STDIN"))
    pickCmd =
      command
        "pick"
        (info
           (helper <*> (Pick <$> pickOpts))
           (progDesc "pick things from a run of `giver give`"))
    giveOpts =
      GiveOpts <$>
      option
        auto
        (long "output-format" <> short 'o' <> value OutIDList <>
         metavar "OUTPUT-FORMAT" <>
         help "The format of the output {json|idlist}")
    pickOpts =
      let ids :: Parser [ID]
          ids =
            fmap
              (numericIDs . unwords)
              (some $
               argument
                 str
                 (metavar "IDs..." <> help "The IDs of things to pick"))
      in PickOpts <$> ids <*>
         option
           auto
           (long "input-format" <> short 'i' <> value InIDList <>
            metavar "INPUT-FORMAT" <>
            help "The format of the input {json|idlist}") <*>
         option
           auto
           (long "output-format" <> short 'o' <> value OutBare <>
            metavar "OUTPUT-FORMAT" <>
            help "The format of the output {json|idlist|bare}")
