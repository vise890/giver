module Opts.Parsers.IDs
  ( numericIDs
  ) where

import Data.Text (Text)
import qualified Data.Text as T

import Data.Attoparsec.Text

import Opts.Types

numericIDs :: String -> [ID]
numericIDs src =
  case parseIDTokens $ T.pack src of
    Left _ -> []
    Right ids -> ids >>= expandNumericID

parseIDTokens :: Text -> Either String [IDToken]
parseIDTokens = parseOnly (idToken `sepBy` space <* endOfInput)

expandNumericID :: IDToken -> [ID]
expandNumericID (SingleID i) = [i]
expandNumericID (IDRange start end) = [start .. end]

type Start = Int

type End = Int

data IDToken
  = SingleID ID
  | IDRange Start
            End
  deriving (Show, Eq)

idToken :: Parser IDToken
idToken = choice $ map try [idRange, singleID]

singleID :: Parser IDToken
singleID = fmap (SingleID . read) (many1 digit)

idRange :: Parser IDToken
idRange = do
  start <- many1 digit
  _ <- char '-'
  end <- many1 digit
  return $ IDRange (read start) (read end)
