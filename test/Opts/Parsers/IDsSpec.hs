module Opts.Parsers.IDsSpec where

import Opts.Parsers.IDs
import Test.Hspec

spec :: Spec
spec =
  describe "numeric IDs" $ do
    it "parses single IDs" $ numericIDs "1 2 42" `shouldBe` [1, 2, 42]
    it "parses and expands ranges" $
      numericIDs "1-3 4-6" `shouldBe` [1, 2, 3, 4, 5, 6]
    it "deals with mixes of single IDs and ranges" $
      numericIDs "1 3-5 42" `shouldBe` [1, 3, 4, 5, 42]
