module Opts.TypesSpec where

import Control.Exception (evaluate)

import Test.Hspec

import Opts.Types

spec :: Spec
spec = do
  describe "InFormat" $ do
    describe "the Read instance" $ do
      it "reads simple strings" $ do
        read "json" `shouldBe` InJSON
        read "idlist" `shouldBe` InIDList
      it "blows up on invalid input" $
        -- TODO: is there no cleaner way of doing this?
        evaluate (read "jsn" :: InFormat) `shouldThrow`
        errorCall "Prelude.read: no parse"
    describe "the Show instance" $
      it "faithfully renders strings representing the value" $ do
        show InJSON `shouldBe` "json"
        show InIDList `shouldBe` "idlist"
  describe "OutFormat" $ do
    describe "the Read instance" $ do
      it "reads simple strings" $ do
        read "json" `shouldBe` OutJSON
        read "idlist" `shouldBe` OutIDList
        read "bare" `shouldBe` OutBare
      it "blows up on invalid input" $
        -- TODO: is there no cleaner way of doing this?
        evaluate (read "jsn" :: OutFormat) `shouldThrow`
        errorCall "Prelude.read: no parse"
    describe "the Show instance" $
      it "faithfully renders strings representing the value" $ do
        show OutJSON `shouldBe` "json"
        show OutIDList `shouldBe` "idlist"
        show OutBare `shouldBe` "bare"
