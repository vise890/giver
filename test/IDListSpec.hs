{-# LANGUAGE OverloadedStrings #-}

module IDListSpec where

import Test.Hspec

import IDList

spec :: Spec
spec =
  describe "reading an IDList" $ do
    it "works on a simple json input" $
      readJSONIDList "{\"1\": \"foo\"}" `shouldBe` Right [(1, "foo")]
    it "works on a simple IDList input" $
      readIDList "1 foo\n2 bar" `shouldBe` Right [(1, "foo"), (2, "bar")]
